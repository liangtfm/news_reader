class FeedsController < ApplicationController
  def index
    @feeds = current_user.feeds

    @feeds.each do |feed|
      if feed.entries.length > 0
        last_update_time = feed.entries.last.updated_at
        if ((Time.now - last_update_time) > 120)
          feed.reload
        end
      else
        feed.reload
      end
    end

    if request.xhr?
      render :json => @feeds.to_json(:include => :entries)
    end
    # respond_to do |format|
#       format.html { render :index }
#       format.json { render :json => @feeds.to_json(:include => :entries) }
#     end
  end

  def create
    @feed = Feed.find_or_create_by_url(params[:feed][:url], current_user.id)

    if request.xhr?
      if @feed
        render :json => @feed
      else
        render :json => { error: "invalid url" }, status: :unprocessable_entity
      end
    else
      redirect_to feeds_url
    end
  end

  def show
    @feed = Feed.find(params[:id])
    @feed.reload

    if request.xhr?
      render json: @feed.to_json(:include => :entries) # {@feed, @entries}
    else
      render :show
    end
  end
end
