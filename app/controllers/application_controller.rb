class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user

  def current_user
    @user ||= User.find_by_session_token(session[:session_token])
  end

  def log_in_user(user)
    user.session_token = SecureRandom::urlsafe_base64(16)
    session[:session_token] = user.session_token
    user.save!
  end


end
