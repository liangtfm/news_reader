class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by_username_and_password(
    params[:user][:username],
    params[:user][:password]
    )

    if @user
      log_in_user(@user)
      redirect_to feeds_url
    else
      flash.now[:errors] = ["WRONG CREDENTIALS"]
      render :new
    end
  end

  def destroy

    session[:session_token] = nil
    redirect_to new_session_url
  end

end
