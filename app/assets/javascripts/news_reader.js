(function(root){

  var NewsReader = root.NewsReader = (root.NewsReady || {});

  NewsReader.Models = {};
  NewsReader.Collections = {};
  NewsReader.Routers = {};
  NewsReader.Views = {};
  NewsReader.initialize = function() {

    NewsReader.feeds = new NewsReader.Collections.Feeds();

    NewsReader.feeds.fetch({
      success: function () {

        new NewsReader.Routers.FeedsRouter({
          "$rootEl": $("#content")
        });

        Backbone.history.start();
      }
    })


  };

})(this);

$(document).ready(function(){

  NewsReader.initialize();

})