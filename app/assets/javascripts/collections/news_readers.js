NewsReader.Collections.Feeds = Backbone.Collection.extend({
  url: "/feeds",
  model: NewsReader.Models.Feed
});

NewsReader.Collections.Entries = Backbone.Collection.extend({
  model: NewsReader.Models.Entry
});