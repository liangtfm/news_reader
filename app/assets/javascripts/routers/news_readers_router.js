NewsReader.Routers.FeedsRouter = Backbone.Router.extend({
  initialize: function(options) {
    this.$rootEl = options.$rootEl;
  },

  routes: {
    "": "index",
    "feeds/:feed_id/entries": "showFeed",
    "feeds/:feed_id/entries/:entry_id": "showEntry"
  },

  index: function() {
    var that = this;

    var indexView = new NewsReader.Views.FeedsIndexView({
      collection: NewsReader.feeds
    });

    that._swapView(indexView);

  },

  showFeed: function(feed_id){
    var that = this;

    that._getFeed(parseInt(feed_id), function(feed) {
      var showView = new NewsReader.Views.FeedShowView({
        model: feed,
        collection: feed.get("entries")
      });

      that._swapView(showView);
    });
  },



  _getFeed: function (id, callback) {
      var feed = NewsReader.feeds.get(id);
      if (!feed) {
        feed = new NewsReader.Models.Feed({ id: id });
        feed.collection = NewsReader.feeds;
        feed.fetch({
          success: function () {
            NewsReader.feeds.add(feed);
            callback(feed);
          }
        });
      } else {
        callback(feed);
      }
    },

  showEntry: function(feed_id, entry_id) {
    var that = this;
    var feed = NewsReader.feeds.get(feed_id);
    var entry = feed.get("entries").get(entry_id);

    var entryView = new NewsReader.Views.EntryShowView({
      model: entry
    });

    that._swapView(entryView);
  },

  _swapView: function (view) {
    this._currentView && this._currentView.remove();
    this._currenView = view;
    this.$rootEl.html(view.render().$el);
  }

});
