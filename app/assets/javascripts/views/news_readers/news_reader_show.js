NewsReader.Views.EntryShowView = Backbone.View.extend({

  template: JST['news_readers/show_entry'],
  render: function(){
    var renderedContent = this.template({
      entry: this.model
    });

    this.$el.html(renderedContent);

    return this;
  }

});
