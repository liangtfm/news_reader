NewsReader.Views.FeedsIndexView = Backbone.View.extend({

  template: JST['news_readers/index'],
  render: function(){
    var renderedContent = this.template({
      feeds: this.collection
    });

    this.$el.html(renderedContent);

    return this;
  }

});
