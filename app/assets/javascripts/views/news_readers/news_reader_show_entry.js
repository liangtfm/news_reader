NewsReader.Views.FeedShowView = Backbone.View.extend({

  template: JST['news_readers/show'],
  render: function(){
    var renderedContent = this.template({
      feed: this.model,
      entries: this.collection
    });

    this.$el.html(renderedContent);

    return this;
  },

  events: {
    'click .refresh_button':'refreshFeeds'
  },

  refreshFeeds: function(event){
    var that = this;

    that.model.fetch({
      success: function(data){
        that.collection = data.get("entries");
        that.render();
      }
    });
  }

});
