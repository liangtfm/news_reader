class User < ActiveRecord::Base
  attr_accessible :username, :password, :session_token

  validates :username, :password, presence: true
  validates :username, uniqueness: true
  validates :password, length: { minimum: 3 }

  has_many :feeds

  def self.find_by_username_and_password(username, password)

    @user = User.find_by_username(username)

    if @user && @user.is_password(password)
      return @user
    else
      return nil
    end
  end

  def is_password(password)
    return (self.password == password)
  end

end
