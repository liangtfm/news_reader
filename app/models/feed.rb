class Feed < ActiveRecord::Base
  attr_accessible :title, :url, :user_id

  validates :title, :url, :user_id, presence: true

  has_many :entries, :dependent => :destroy
  belongs_to :user

  def self.find_or_create_by_url(url, id)
    feed = Feed.find_by_url(url)
    return feed if feed

    begin
      feed_data = SimpleRSS.parse(open(url))
      feed = Feed.create!(title: feed_data.title, url: url, user_id: id)
      feed_data.entries.each do |entry_data|
        Entry.create_from_json!(entry_data, feed)
      end
    rescue SimpleRSSError
      return nil
    end

    feed
  end

  # def as_json(options) # { created_at: ..., id:, ... }
  #   super(options).merge({ entries: self.entries })
  # end

  def reload
    # reloads entries
    self.touch #this causes the updated_at column to be updated
    begin
      feed_data = SimpleRSS.parse(open(url))
      existing_entry_guids = Entry.pluck(:guid).sort
      feed_data.entries.each do |entry_data|
        unless existing_entry_guids.include?(entry_data.guid)
          Entry.create_from_json!(entry_data, self)
        end
      end

      self
    rescue SimpleRSSError
      return false
    end
  end
end
